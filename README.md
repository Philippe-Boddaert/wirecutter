# Wirecutter

## 1. Principe

- Une bombe peut contenir entre 3 et 6 fils,
- Seul un fil a besoin d'être coupé pour désarmer la bombe,
- Les fils sont ordonnés de haut en bas,
- Le joueur a 15 secondes pour couper le bon fil !!

## 2. Règles de désamorçage

Pour déterminer le bon fil à couper, voici les règles, selon le nombre de fils de la bombe :

__3 fils__ :

S'il n'y a pas de fil rouge, couper le deuxième fil.

Sinon, si le dernier fil est blanc, couper le dernier fil.

Sinon, s'il y a plus d'un fil bleu, couper le dernier fil bleu.

Sinon, couper le dernier fil.

__4 fils__ :

S'il y a plus d'un fil rouge, couper le dernier fil rouge.

Sinon, si le dernier fil est jaune et s'il n'y a pas de fil rouge, couper le premier fil.

Sinon, s'il y a exactement un fil bleu, couper le premier fil.

Sinon, s'il y a plus d'un fil jaune, couper le dernier fil.

Sinon, couper le deuxième fil.

__5 fils__ :

Si le dernier fil est vert, couper le quatrième fil.

Sinon, s'il y a exactement un fil rouge et plus d'un fil jaune, couper le premier fil.

Sinon, s'il n'y a pas de fil vert, couper le deuxième fil.

Sinon, couper le premier fil.

__6 fils__ :

S'il n'y a pas de fil jaune, couper le troisième fil.

Sinon, s'il y a exactement un fil jaune et plus d'un fil blanc, couper le quatrième fil.

Sinon, s'il n'y a pas de fil rouge, couper le dernier fil.

Sinon, couper le quatrième fil.